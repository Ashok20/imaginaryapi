package com.imaginary.controller.task;

import com.imaginary.model.hibernate.Task;
import com.imaginary.service.task.TaskService;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TaskController {

    @Autowired
    private TaskService taskService;

    @RequestMapping(value = "/task/create", method = RequestMethod.POST)
    public Map<String, Object> createTask(@RequestParam("project") int project, @RequestParam("employee") int employee,
            @RequestParam("description") String description, @RequestParam("date") String date, @RequestParam("hours") int hours,
            @RequestParam("overtime") int overtime, @RequestParam("status") int status) {
        Map<String, Object> response = new HashMap<>();
        try {
            Task task = new Task();
            task.setProject(project);
            task.setEmployee(employee);
            task.setDescription(description);
            task.setDate(date);
            task.setHours(hours);
            task.setOvertime(overtime);
            task.setStatus(status);
            if (taskService.createTask(task)) {
                response.put("status", 200);
                response.put("success", true);
                response.put("message", "Task Saved");
            } else {
                response.put("status", 500);
                response.put("success", false);
                response.put("message", "Error Saving Task");
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.put("status", 500);
            response.put("success", false);
            response.put("message", e);
        }
        return response;
    }

    @RequestMapping(value = "/task/search", method = RequestMethod.GET)
    public Map<String, Object> getTaskById(@RequestParam("id") int id) {
        Map<String, Object> response = new HashMap<>();
        try {
            Task task = taskService.getTaskById(id);
            response.put("status", 200);
            response.put("success", true);
            response.put("data", task);
        } catch (Exception e) {
            e.printStackTrace();
            response.put("status", 500);
            response.put("success", false);
            response.put("message", e);
        }
        return response;
    }

    @RequestMapping(value = "/tasks", method = RequestMethod.PUT)
    public Map<String, Object> updateTask(@RequestParam("id") int id, @RequestParam("date") String date,
            @RequestParam("overtime") int overtime) {
        Map<String, Object> response = new HashMap<>();
        try {
            Task task = new Task();
            task.setTaskid(id);
            task.setDate(date);
            task.setOvertime(overtime);
            if (taskService.updateTask(task)) {
                response.put("status", 200);
                response.put("success", true);
                response.put("message", "Task ID : " + task.getTaskid() + " updated");
            } else {
                response.put("status", 500);
                response.put("success", false);
                response.put("message", "Error Updating Task ID : " + task.getTaskid());
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.put("status", 500);
            response.put("success", false);
            response.put("message", e);
        }
        return response;
    }

    @RequestMapping(value = "/task/delete", method = RequestMethod.DELETE)
    public Map<String, Object> deleteTaskById(@RequestParam("id") int id) {
        Map<String, Object> response = new HashMap<>();
        try {
            if (taskService.deleteTaskById(id)) {
                response.put("status", 200);
                response.put("success", true);
                response.put("message", "Task ID : " + id + " Deleted");
            } else {
                response.put("status", 500);
                response.put("success", false);
                response.put("message", "Error Deleting Task ID : " + id);
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.put("status", 500);
            response.put("success", false);
            response.put("message", e);
        }
        return response;
    }
}
