package com.imaginary.controller.project;

import com.imaginary.model.hibernate.Project;
import com.imaginary.service.project.ProjectService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @RequestMapping(value = "/project/create", method = RequestMethod.POST)
//    public Map<String, Object> createProject(@ModelAttribute("project") Project project) {
    public Map<String, Object> createProject(@RequestParam("description") String description, @RequestParam("manager") int manager, @RequestParam("status") int status) {
        Map<String, Object> response = new HashMap<>();
        try {
            Project project = new Project();
            project.setDescription(description);
            project.setManager(manager);
            project.setStatus(status);
            if (projectService.createProject(project)) {
                response.put("status", 200);
                response.put("success", true);
                response.put("message", "Project Saved");
            } else {
                response.put("status", 500);
                response.put("success", false);
                response.put("message", "Error Saving Project");
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.put("status", 500);
            response.put("success", false);
            response.put("message", e);
        }
        return response;
    }

    @RequestMapping(value = "/projects", method = RequestMethod.GET)
    public Map<String, Object> getProjectInfoByManagerId(@RequestParam("id") int employeeId) {
        Map<String, Object> response = new HashMap<>();
        try {
            List<Map<String, Object>> data = projectService.getProjectInfoByManagerId(employeeId);
            response.put("status", 200);
            response.put("success", true);
            response.put("message", "Project Saved");
            response.put("data", data);
        } catch (Exception e) {
            e.printStackTrace();
            response.put("status", 500);
            response.put("success", false);
            response.put("message", e);
        }
        return response;
    }

    @RequestMapping(value = "/projectdropdown", method = RequestMethod.GET)
    public Map<String, Object> getProjectDropdownData() {
        Map<String, Object> response = new HashMap<>();
        try {
            List<Map<String, Object>> data = projectService.getProjectDropdownData();
            response.put("status", 200);
            response.put("success", true);
            response.put("message", "Projects Retrived Successfully");
            response.put("data", data);
        } catch (Exception e) {
            e.printStackTrace();
            response.put("status", 500);
            response.put("success", false);
            response.put("message", e);
        }
        return response;
    }
    
    @RequestMapping(value = "/statusdropdown", method = RequestMethod.GET)
    public Map<String, Object> getStatusDropdownData() {
        Map<String, Object> response = new HashMap<>();
        try {
            List<Map<String, Object>> data = projectService.getStatusDropdownData();
            response.put("status", 200);
            response.put("success", true);
            response.put("message", "Status Retrived Successfully");
            response.put("data", data);
        } catch (Exception e) {
            e.printStackTrace();
            response.put("status", 500);
            response.put("success", false);
            response.put("message", e);
        }
        return response;
    }
}
