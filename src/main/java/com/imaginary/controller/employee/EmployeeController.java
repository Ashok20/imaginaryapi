package com.imaginary.controller.employee;

import com.imaginary.model.hibernate.Employee;
import com.imaginary.service.employee.EmployeeService;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {
    
    @Autowired
    private EmployeeService employeeService;
    
    @RequestMapping(value = "/employee/create", method = RequestMethod.POST)
//    public Map<String, Object> createEmployee(@RequestParam("employee") Employee employee) {
    public Map<String, Object> createEmployee(@RequestParam("name") String name, @RequestParam("nic") String nic,
            @RequestParam("contact") String contact, @RequestParam("userrole") String userrole) {
        Map<String, Object> response = new HashMap<>();
        try {
            Employee employee = new Employee();
            employee.setName(name);
            employee.setNic(nic);
            employee.setContact(contact);
            employee.setUserrole(userrole);
            if (employeeService.createEmployee(employee)) {
                response.put("status", 200);
                response.put("success", true);
                response.put("message", "Employee Saved");
            } else {
                response.put("status", 500);
                response.put("success", false);
                response.put("message", "Error Saving Employee");
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.put("status", 500);
            response.put("success", false);
            response.put("message", e);
        }
        return response;
    }
    
    @RequestMapping(value = "/employee", method = RequestMethod.POST)
    public Map<String, Object> getEmployeeById(@RequestParam("id") int employeeId) {
        Map<String, Object> response = new HashMap<>();
        try {
            Employee employee = employeeService.getEmployee(employeeId);
            response.put("status", 200);
            response.put("success", true);
            response.put("data", employee);
        } catch (Exception e) {
            e.printStackTrace();
            response.put("status", 500);
            response.put("success", false);
            response.put("message", e);
        }
        return response;
    }
    
    @RequestMapping(value = "/developers", method = RequestMethod.GET)
    public Map<String, Object> getOvertimeById(@RequestParam("id") int employeeId) {
        Map<String, Object> response = new HashMap<>();
        try {
            Long overTime = employeeService.getOvertimeById(employeeId);
            Map<String, Object> data = new HashMap();
            data.put("overtime", overTime);
            response.put("status", 200);
            response.put("success", true);
            response.put("data", data);
        } catch (Exception e) {
            e.printStackTrace();
            response.put("status", 500);
            response.put("success", false);
            response.put("message", e);
        }
        return response;
    }
    
    @RequestMapping(value = "/employeedropdown", method = RequestMethod.GET)
    public Map<String, Object> getEmployeeDropdownData() {
        Map<String, Object> response = new HashMap<>();
        try {
            List<Map<String, Object>> data = employeeService.getEmployeeDropdownData();
            response.put("status", 200);
            response.put("success", true);
            response.put("message", "Employee Retrived Successfully");
            response.put("data", data);
        } catch (Exception e) {
            e.printStackTrace();
            response.put("status", 500);
            response.put("success", false);
            response.put("message", e);
        }
        return response;
    }
}
