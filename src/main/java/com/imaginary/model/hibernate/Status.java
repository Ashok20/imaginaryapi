package com.imaginary.model.hibernate;
// Generated May 2, 2018 11:09:16 PM by Hibernate Tools 4.3.1


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Status generated by hbm2java
 */
@Entity
@Table(name="status"
    ,catalog="imaginary"
)
public class Status  implements java.io.Serializable {


     private Integer statusid;
     private String description;

    public Status() {
    }

    public Status(String description) {
       this.description = description;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="STATUSID", unique=true, nullable=false)
    public Integer getStatusid() {
        return this.statusid;
    }
    
    public void setStatusid(Integer statusid) {
        this.statusid = statusid;
    }

    
    @Column(name="DESCRIPTION", nullable=false, length=20)
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }




}


