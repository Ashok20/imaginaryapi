package com.imaginary.model.hibernate;
// Generated May 2, 2018 11:09:16 PM by Hibernate Tools 4.3.1


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Task generated by hbm2java
 */
@Entity
@Table(name="task"
    ,catalog="imaginary"
)
public class Task  implements java.io.Serializable {


     private Integer taskid;
     private int project;
     private int employee;
     private String description;
     private String date;
     private Integer hours;
     private Integer overtime;
     private int status;

    public Task() {
    }

	
    public Task(int project, int employee, String description, int status) {
        this.project = project;
        this.employee = employee;
        this.description = description;
        this.status = status;
    }
    public Task(int project, int employee, String description, String date, Integer hours, Integer overtime, int status) {
       this.project = project;
       this.employee = employee;
       this.description = description;
       this.date = date;
       this.hours = hours;
       this.overtime = overtime;
       this.status = status;
    }
   
     @Id @GeneratedValue(strategy=IDENTITY)

    
    @Column(name="TASKID", unique=true, nullable=false)
    public Integer getTaskid() {
        return this.taskid;
    }
    
    public void setTaskid(Integer taskid) {
        this.taskid = taskid;
    }

    
    @Column(name="PROJECT", nullable=false)
    public int getProject() {
        return this.project;
    }
    
    public void setProject(int project) {
        this.project = project;
    }

    
    @Column(name="EMPLOYEE", nullable=false)
    public int getEmployee() {
        return this.employee;
    }
    
    public void setEmployee(int employee) {
        this.employee = employee;
    }

    
    @Column(name="DESCRIPTION", nullable=false, length=200)
    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }

    
    @Column(name="DATE", length=20)
    public String getDate() {
        return this.date;
    }
    
    public void setDate(String date) {
        this.date = date;
    }

    
    @Column(name="HOURS")
    public Integer getHours() {
        return this.hours;
    }
    
    public void setHours(Integer hours) {
        this.hours = hours;
    }

    
    @Column(name="OVERTIME")
    public Integer getOvertime() {
        return this.overtime;
    }
    
    public void setOvertime(Integer overtime) {
        this.overtime = overtime;
    }

    
    @Column(name="STATUS", nullable=false)
    public int getStatus() {
        return this.status;
    }
    
    public void setStatus(int status) {
        this.status = status;
    }




}


