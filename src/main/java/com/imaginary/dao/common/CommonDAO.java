package com.imaginary.dao.common;

import java.util.List;
import java.util.Map;

public interface CommonDAO {
    
    public List<Map<String, Object>> getDropdownValueList(String query) throws Exception;
    
}
