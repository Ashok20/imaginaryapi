package com.imaginary.dao.common;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class CommonDAOImpl implements CommonDAO {

    @Autowired
    private DataSource dataSource;

    @Override
    public List<Map<String, Object>> getDropdownValueList(String query) throws SQLException, DataAccessException {
        return new JdbcTemplate(dataSource).query(query, new DropdownRowMapper());
    }

    public class DropdownRowMapper implements RowMapper<Map<String, Object>> {

        @Override
        public Map<String, Object> mapRow(ResultSet rs, int i) throws SQLException {
            Map<String, Object> data = new HashMap<>();
            data.put("id", rs.getString("ID"));
            data.put("value", rs.getString("DESCRIPTION"));
            return data;
        }
    }    
}
