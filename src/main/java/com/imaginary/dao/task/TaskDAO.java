package com.imaginary.dao.task;

import com.imaginary.model.hibernate.Task;

public interface TaskDAO {

    public Task getTaskById(int taskId) throws Exception;

    public boolean createTask(Task task) throws Exception;

    public boolean updateTask(Task task) throws Exception;

    public boolean deleteTaskById(int taskId) throws Exception;
}
