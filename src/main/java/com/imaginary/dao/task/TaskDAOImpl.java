package com.imaginary.dao.task;

import com.imaginary.model.hibernate.Task;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("taskDAO")
public class TaskDAOImpl implements TaskDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Task getTaskById(int taskId) throws Exception {
        Session session = sessionFactory.getCurrentSession();
        return (Task) session.get(Task.class, taskId);
    }

    @Override
    public boolean createTask(Task task) throws Exception {
        boolean status = true;
        try {
            Session session = sessionFactory.getCurrentSession();
            session.persist(task);
        } catch (HibernateException e) {
            status = false;
            throw e;
        }
        return status;
    }

    @Override
    public boolean updateTask(Task task) throws Exception {
        boolean status = true;
        try {
            Session session = sessionFactory.getCurrentSession();
            Task oldTask = this.getTaskById(task.getTaskid());
            oldTask.setDate(task.getDate());
            oldTask.setOvertime(task.getOvertime());
            session.merge(oldTask);
        } catch (HibernateException e) {
            status = false;
            throw e;
        }
        return status;
    }

    @Override
    public boolean deleteTaskById(int taskId) throws Exception {
        boolean status = true;
        try {
            Session session = sessionFactory.getCurrentSession();
            Task task = this.getTaskById(taskId);
            session.delete(task);
        } catch (HibernateException e) {
            status = false;
            throw e;
        }
        return status;
    }

}
