package com.imaginary.dao.project;

import com.imaginary.model.hibernate.Project;
import java.util.List;
import java.util.Map;

public interface ProjectDAO {

    public boolean createProject(Project project) throws Exception;

    public List<Map<String, Object>> getProjectInfoByManagerId(int employeeId) throws Exception;
}
