package com.imaginary.dao.project;

import com.imaginary.model.hibernate.Project;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository("projectDAO")
public class ProjectDAOImpl implements ProjectDAO {

    @Autowired
    private SessionFactory sessionFactory;
    @Autowired
    private DataSource dataSource;

    @Override
    public boolean createProject(Project project) throws Exception {
        boolean status = true;
        try {
            Session session = sessionFactory.getCurrentSession();
            session.persist(project);
        } catch (HibernateException e) {
            status = false;
            throw e;
        }
        return status;
    }

    @Override
    public List<Map<String, Object>> getProjectInfoByManagerId(int employeeId) throws Exception {
        String query = "SELECT "
                + "DISTINCT(T.EMPLOYEE) AS EMPLOYEE, "
                + "P.DESCRIPTION AS PROJECT, "
                + "E2.NAME AS MEMBER, "
                + "(SELECT SUM(T2.HOURS) FROM task T2 WHERE T2.EMPLOYEE = T.EMPLOYEE AND T2.PROJECT = T.PROJECT) AS HOURS, "
                + "(SELECT SUM(T2.OVERTIME) FROM task T2 WHERE T2.EMPLOYEE = T.EMPLOYEE AND T2.PROJECT = T.PROJECT) AS OVERTIME, "
                + "((SELECT SUM(T2.HOURS) FROM task T2 WHERE T2.EMPLOYEE = T.EMPLOYEE AND T2.PROJECT = T.PROJECT) / (SELECT SUM(T2.HOURS) FROM task T2 WHERE T2.PROJECT = T.PROJECT)*100) AS CONTRIBUTION "
                + "FROM task T "
                + "LEFT JOIN project P ON P.PROJECTID = T.PROJECT "
                + "LEFT JOIN employee E1 ON E1.EMPLOYEEID = P.MANAGER "
                + "LEFT JOIN employee E2 ON E2.EMPLOYEEID = T.EMPLOYEE "
                + "WHERE E1.EMPLOYEEID = ? "
                + "ORDER BY T.PROJECT, T.EMPLOYEE ";
        return new JdbcTemplate(dataSource).query(query, new Object[]{employeeId}, new ProjectInfoViewRowMapper());
    }

    public class ProjectInfoViewRowMapper implements RowMapper<Map<String, Object>> {

        @Override
        public Map<String, Object> mapRow(ResultSet rs, int i) throws SQLException {
            Map<String, Object> object = new HashMap<>();
            object.put("employeeid", rs.getString("EMPLOYEE"));
            object.put("project", rs.getString("PROJECT"));
            object.put("member", rs.getString("MEMBER"));
            object.put("hours", rs.getString("HOURS"));
            object.put("overtime", rs.getString("OVERTIME"));
            object.put("contribution", rs.getString("CONTRIBUTION"));
            return object;
        }
    }
}
