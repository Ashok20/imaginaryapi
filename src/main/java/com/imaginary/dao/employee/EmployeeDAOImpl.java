package com.imaginary.dao.employee;

import com.imaginary.model.hibernate.Employee;
import com.imaginary.model.hibernate.Task;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository("employeeDAO")
public class EmployeeDAOImpl implements EmployeeDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public Employee getEmployee(int employeeId) throws Exception {
        Session session = sessionFactory.getCurrentSession();
        return (Employee) session.get(Employee.class, employeeId);
    }

    @Override
    public List<Employee> getEmployees() throws Exception {
        Session session = sessionFactory.getCurrentSession();
        Criteria criteria = session.createCriteria(Employee.class);
        return (List<Employee>) criteria.list();
    }

    @Override
    public boolean createEmployee(Employee employee) throws Exception {
        boolean status = true;
        try {
            Session session = sessionFactory.getCurrentSession();
            session.persist(employee);
        } catch (HibernateException e) {
            status = false;
            throw e;
        }
        return status;
    }

    @Override
    public boolean createEmployees(List<Employee> employees) throws Exception {
        boolean status = true;
        try {
            Session session = sessionFactory.getCurrentSession();
            for (Employee employee : employees) {
                session.persist(employee);
            }
        } catch (HibernateException e) {
            status = false;
            throw e;
        }
        return status;
    }

    @Override
    public boolean deleteEmployee(int employeeId) throws Exception {
        boolean status = true;
        try {
            Session session = sessionFactory.getCurrentSession();
            Employee employee = (Employee) session.load(Employee.class, employeeId);
            session.delete(employee);
        } catch (HibernateException e) {
            status = false;
            throw e;
        }
        return status;
    }

    @Override
    public Long getOvertimeById(int employeeId) throws Exception {               
        Session session = sessionFactory.getCurrentSession();
        return (Long) session.createCriteria(Task.class)
        .setProjection(Projections.sum("overtime"))
        .add(Restrictions.eq("employee", employeeId))
        .uniqueResult();
    }
}
