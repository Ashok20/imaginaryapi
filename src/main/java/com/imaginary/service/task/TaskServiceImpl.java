package com.imaginary.service.task;

import com.imaginary.dao.common.CommonDAOImpl;
import com.imaginary.dao.task.TaskDAO;
import com.imaginary.model.hibernate.Task;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("taskService")
public class TaskServiceImpl implements TaskService {

    @Autowired
    private TaskDAO taskDAO;

    @Override
    @Transactional
    public boolean createTask(Task task) throws Exception {
        return taskDAO.createTask(task);
    }

    @Override
    @Transactional
    public boolean updateTask(Task task) throws Exception {
        return taskDAO.updateTask(task);
    }

    @Override
    @Transactional
    public boolean deleteTaskById(int taskId) throws Exception {
        return taskDAO.deleteTaskById(taskId);
    }
    
    @Override
    @Transactional
    public Task getTaskById(int taskId) throws Exception {
        return taskDAO.getTaskById(taskId);
    }
}
