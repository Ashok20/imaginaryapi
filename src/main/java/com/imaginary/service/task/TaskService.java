package com.imaginary.service.task;

import com.imaginary.model.hibernate.Task;

public interface TaskService {

    public boolean createTask(Task task) throws Exception;

    public boolean updateTask(Task task) throws Exception;

    public boolean deleteTaskById(int taskId) throws Exception;

    public Task getTaskById(int taskId) throws Exception;
}
