package com.imaginary.service.employee;

import com.imaginary.model.hibernate.Employee;
import java.util.List;
import java.util.Map;

public interface EmployeeService {

    public Employee getEmployee(int employeeId) throws Exception;

    public List<Employee> getEmployees() throws Exception;

    public boolean createEmployee(Employee employee) throws Exception;

    public boolean createEmployees(List<Employee> employees) throws Exception;

    public boolean deleteEmployee(int employeeId) throws Exception;
    
    public Long getOvertimeById(int employeeId) throws Exception;
    
    public List<Map<String, Object>> getEmployeeDropdownData() throws Exception;

}
