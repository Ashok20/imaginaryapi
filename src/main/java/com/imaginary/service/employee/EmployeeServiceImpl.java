package com.imaginary.service.employee;

import com.imaginary.dao.common.CommonDAO;
import com.imaginary.dao.employee.EmployeeDAO;
import com.imaginary.model.hibernate.Employee;
import java.util.List;
import java.util.Map;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("employeeService")
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeDAO employeeDAO;
    @Autowired
    private CommonDAO commonDAO;

    @Override
    @Transactional
    public Employee getEmployee(int employeeId) throws Exception {
        return employeeDAO.getEmployee(employeeId);
    }

    @Override
    @Transactional
    public List<Employee> getEmployees() throws Exception {
        return employeeDAO.getEmployees();
    }

    @Override
    @Transactional//("sessionFactoryTransaction")
    public boolean createEmployee(Employee employee) throws Exception {
        return employeeDAO.createEmployee(employee);
    }

    @Override
    @Transactional
    public boolean createEmployees(List<Employee> employees) throws Exception {
        return employeeDAO.createEmployees(employees);
    }

    @Override
    @Transactional
    public boolean deleteEmployee(int employeeId) throws Exception {
        return employeeDAO.deleteEmployee(employeeId);
    }

    @Override
    @Transactional//("sessionFactoryTransaction")
    public Long getOvertimeById(int employeeId) throws Exception {
        return employeeDAO.getOvertimeById(employeeId);
    }

    @Override
    @Transactional
    public List<Map<String, Object>> getEmployeeDropdownData() throws Exception {
        String sql = "SELECT EMPLOYEEID AS ID, NAME AS DESCRIPTION FROM EMPLOYEE WHERE USERROLE = 'developer' ";
        return commonDAO.getDropdownValueList(sql);
    }
}
