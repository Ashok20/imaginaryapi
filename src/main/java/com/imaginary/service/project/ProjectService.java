package com.imaginary.service.project;

import com.imaginary.model.hibernate.Project;
import java.util.List;
import java.util.Map;

public interface ProjectService {

    public boolean createProject(Project project) throws Exception;

    public List<Map<String, Object>> getProjectInfoByManagerId(int employeeId) throws Exception;
    
    public List<Map<String, Object>> getProjectDropdownData() throws Exception;
    
    public List<Map<String, Object>> getStatusDropdownData() throws Exception;
}
