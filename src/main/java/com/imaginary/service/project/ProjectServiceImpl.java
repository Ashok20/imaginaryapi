package com.imaginary.service.project;

import com.imaginary.dao.common.CommonDAO;
import com.imaginary.dao.project.ProjectDAO;
import com.imaginary.model.hibernate.Project;
import java.util.List;
import java.util.Map;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("projectService")
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private ProjectDAO projectDAO;
    @Autowired
    private CommonDAO commonDAO;

    @Override
    @Transactional
    public boolean createProject(Project project) throws Exception {
        return projectDAO.createProject(project);
    }

    @Override
    @Transactional
    public List<Map<String, Object>> getProjectInfoByManagerId(int employeeId) throws Exception {
        return projectDAO.getProjectInfoByManagerId(employeeId);
    }

    @Override
    @Transactional
    public List<Map<String, Object>> getProjectDropdownData() throws Exception {
        String sql = "SELECT PROJECTID AS ID, DESCRIPTION FROM PROJECT ";
        return commonDAO.getDropdownValueList(sql);
    }

    @Override
    @Transactional
    public List<Map<String, Object>> getStatusDropdownData() throws Exception {
        String sql = "SELECT STATUSID AS ID, DESCRIPTION FROM STATUS ";
        return commonDAO.getDropdownValueList(sql);
    }
}
